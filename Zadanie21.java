package com.company;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Zadanie21 {
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        System.out.println("Введите число в бинарном формате и нажмите ввод=");
        String str=in.next();
        String regex = "[01]+";
        Pattern pattern = Pattern.compile((regex));
        Matcher matcher = pattern.matcher(str);
        if (!matcher.matches()) {throw new RuntimeException("Не бинарное представление"); }
        int res=convertBinTodec(str);
        System.out.println("Представление в десятичном формате="+ res);
        in.close();
    }
    public static int powCustom(int a,int b){
        int res=1;
        for(int i=0;i<b;i++){
            res*=a;
        }
        return res;
    }
    public  static int convertBinTodec(String str){

        int res=0,a=0,mult=0;
        char[] sym=str.toCharArray();
        for(int len=sym.length-1;len>=0;len--){
            int var=0;
            a=getNumValueChar((sym[len]));
            var=a*powCustom(2,mult);
            mult++;
            res+=var;
        }
        return res;
    }
    private static int getNumValueChar(char c) {
        if (c == '1') return 1;
        else return 0;
    }
}
