package com.company;
import java.util.Arrays;
import java.util.Scanner;
public class zadanie15 {
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        System.out.println("Введите размер массива");
        int len=in.nextInt();
        int[] mas=new int[len];
        int elem;
        System.out.println("Введите элементы массива");
        for (int i=0; i<len;i++){
            elem=in.nextInt();
            mas[i]=elem;}
        System.out.println("Отсортированный  массив");
        boolean isSorted = false;
        int buf;
        int j=mas.length-1;
        while(!isSorted) {
            isSorted = true;
            for (int i = 0; i < j; i++) {
                if(mas[i] > mas[i+1]){
                    isSorted = false;

                    buf = mas[i];
                    mas[i] = mas[i+1];
                    mas[i+1] = buf;
                }
            }
            j--;
        }
        System.out.println(Arrays.toString(mas));
    }
}