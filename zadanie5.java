package com.company;
import java.util.Scanner;
public class zadanie5 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите число от 1 до 10");
        int s = in.nextInt();
        multTable(s, 10);
        in.close();
    }

    public static void multTable(int a, int b) {

        if (b >= 0) {
            multTable(a, b - 1);
            System.out.println(a + " * " +b+ " = " + a * b);
        }
    }
}
