package com.company;

import java.util.Scanner;

public class Zadanie22 {
    static public void main(String[] arg) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите размер массива");
        int num = in.nextInt();
        int[] array = new int[num];
        System.out.println("Введите элементы массива");
        for (int i = 0; i < num; i++) array[i] = in.nextInt();
        sortInsertion(array);
        System.out.println("Отсортированный массив");
        System.out.print("[");
        for (int i = 0; i < num; i++)
            System.out.print(array[i] + " ");
        System.out.print("]");
    }
    private static void sortInsertion(int[] array) {
        int i, j, exchange;
        for (i = 1; i<array.length; i++){
            exchange = array[i];
            j=i;
            while (j>0 && array[j-1]>exchange){
                array[j]=array[j-1];
                j--;
            }
            array[j]= exchange;
        }
    }
}

