package com.company;

import java.util.Scanner;

public class zadanie9 {
    static public void main(String[] arg) {
        Scanner in=new Scanner(System.in);
        System.out.println("Введите размер массива");
        int len=in.nextInt();
        int[] mas=new int[len];
        int elem;
        System.out.println("Введите элементы массива");
        for (int i=0; i<len;i++){
            elem=in.nextInt();
            mas[i]=elem;}
        System.out.println("Исходный массив");
        for(int i=0; i<len;i++) System.out.print(mas[i]+" ");
        for (int i=0; i<len;i++){
            mas[i]*=2;}
        System.out.println();
        System.out.println("Преобразованный массив");
        for(int i=0; i<len;i++) System.out.print(mas[i]+" ");
    }
}
