
package com.company;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Zadanie18 {
    public static void main(String[] arg) throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите путь до файла");
        String str=scanner.nextLine();

        Path path=Paths.get(str);
        List<String> fileContent = new ArrayList<>(Files.readAllLines(path, StandardCharsets.UTF_8));
        int i = 1;
        System.out.println("Исходное содержимое файла");
        for (String key : fileContent) {
            System.out.println("Строка " + i + " " + key);
            i++;
        }
        i = 0;
        System.out.println("Введите новые строки для файла");
        for (String key : fileContent) {
            System.out.println("Новая строка " + (i + 1));
            fileContent.set(i, scanner.nextLine());
            i++;
        }
        i = 1;
        System.out.println("Изменённое содержимое файла");
        for (String key : fileContent) {
            System.out.println("Строка " + i + " " + key);
            i++;
        }
        Files.write(path, fileContent, StandardCharsets.UTF_8);
    }
}


