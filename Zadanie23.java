package com.company;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Scanner;

public class Zadanie23 {
    static public void main(String[] arg) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите курс");
        double cur = in.nextDouble();
        System.out.println("Введите сумму в рублях");
        double sum = in.nextDouble();
        BigDecimal resprom = new BigDecimal(sum / cur);
        DecimalFormat format = new DecimalFormat("#0.00");
        double res = resprom.setScale(2, RoundingMode.HALF_UP).doubleValue();
        System.out.println("Сумма в долларах = " + format.format(res));
    }
}