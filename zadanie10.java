package com.company;

import java.util.Scanner;

public class zadanie10 {
    static public void main(String[] arg) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите число строк массива");
        int n = in.nextInt();
        System.out.println("Введите число столбцов массива");
        int m = in.nextInt();
        int elem;
        int[][] mas = new int[n][m];
        System.out.println("Введите элементы массива");
        for (int i = 0; i < n; i++) {
            System.out.println("Введите элементы " + i + " ой строки");
            for (int j = 0; j < m; j++) {
                elem = in.nextInt();
                mas[i][j] = elem;
            }
        }
        System.out.println("Исходный массив");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++)
                System.out.print(mas[i][j] + " ");
            System.out.print("\n");
        }
        System.out.println("Преобразованная первая строка");
        for (int j = 0; j < m; j++) {
            mas[0][j] *= 3;
            System.out.print(" " + mas[0][j]);
        }

    }
}