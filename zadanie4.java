package com.company;
import java.util.Scanner;
public class zadanie4{
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        System.out.println("Введите число в бинарном формате и нажмите ввод=");
        String str=in.next();
        int res=convertBinTodec(str);
        System.out.println("Представление в десятичном формате="+ res);
        in.close();
    }
    public static int powCustom(int a,int b){
        int res=1;
        for(int i=0;i<b;i++){
            res*=a;
        }
        return res;
    }


    public  static int convertBinTodec(String str){

        int res=0,a=0,mult=0;
        char[] sym=str.toCharArray();
        for(int len=sym.length-1;len>=0;len--){
            int var=0;
            a=Character.getNumericValue(sym[len]);
            var=a*powCustom(2,mult);
            mult++;
            res+=var;
        }
        return res;
    }
}